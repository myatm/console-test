﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyATMReader;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            var reader = new Reader("com3", "MYATM126");
            var state = reader.Start();

            if (state.code != 0) {
                throw new SystemException("error");
            }
        }
    }
}
